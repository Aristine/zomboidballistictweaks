if getActivatedMods():contains("ari_content_tweaker") then
  require("content_tweaker");
else return end

local function TweakItems()

  --vanilla helmets
  ContentTweaker.SetItemParam("BulletDefense", "Base.Hat_Army", 65)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Hat_SPHhelmet", 75)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Hat_RiotHelmet", 30)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Hat_CrashHelmet_Police", 20)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Hat_CrashHelmetFULL", 12)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Hat_CrashHelmet", 10)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Hat_CrashHelmet_Stars", 10)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Vest_BulletCivilian", 65) --default 30 bite, 55 scratch
  ContentTweaker.SetItemParam("ScratchDefense", "Base.Vest_BulletCivilian", 40)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Vest_BulletPolice", 80)
  ContentTweaker.SetItemParam("Weight", "Base.Vest_BulletPolice", 1.5)
  ContentTweaker.SetItemParam("BiteDefense", "Base.Vest_BulletPolice", 45)
  ContentTweaker.SetItemParam("ScratchDefense", "Base.Vest_BulletPolice", 55)
  ContentTweaker.SetItemParam("BulletDefense", "Base.Vest_BulletArmy", 95)
  ContentTweaker.SetItemParam("Weight", "Base.Vest_BulletArmy", 2.5)
  ContentTweaker.SetItemParam("BiteDefense", "Base.Vest_BulletArmy", 60)
  ContentTweaker.SetItemParam("ScratchDefense", "Base.Vest_BulletArmy", 70)
  
  -- unused example
  -- ContentTweaker.SetItemParam("DisplayName", "Base.Hat_Army", "Really Cool Military Helmet")

  --USMC Armory (https://steamcommunity.com/sharedfiles/filedetails/?id=2611652130)
  -- interceptor_armor = armor_s w/ shoulderpads arm protection (best)
  -- interceptor_armor_s = armor_v w/ groin (second best)
  -- interceptor_armor_v = vest & neck (third best)
  if getActivatedMods():contains("DRK_1") then
    ContentTweaker.SetItemParam("BulletDefense","Base.Hat_TPASGT_Helmet", 70)
    ContentTweaker.SetItemParam("BulletDefense","Base.Hat_PASGT_Helmet", 70)
    ContentTweaker.SetItemParam("BulletDefense","Base.Interceptor_Armor", 95) -- no point of it being over 100, it's just misleading as this is percentage of damage blocked
    ContentTweaker.SetItemParam("BulletDefense","Base.Interceptor_Armor_s", 95)
    ContentTweaker.SetItemParam("BulletDefense","Base.Interceptor_Armor_v", 95)
    ContentTweaker.SetItemParam("BiteDefense", "Base.Interceptor_Armor_v", 65)
    ContentTweaker.SetItemParam("ScratchDefense", "Base.Interceptor_Armor_v", 70)
    ContentTweaker.SetItemParam("BiteDefense", "Interceptor_Armor_s", 65)
    ContentTweaker.SetItemParam("ScratchDefense", "Interceptor_Armor_s", 70)    
    ContentTweaker.SetItemParam("BiteDefense", "Base.Interceptor_Armor", 65)
    ContentTweaker.SetItemParam("ScratchDefense", "Base.Interceptor_Armor", 70)      
    ContentTweaker.SetItemParam("Capacity", "Base.Interceptor_Pouches", 10) 
    ContentTweaker.SetItemParam("Capacity", "Interceptor_Pouches_Straps", 10) 
  end
end

local function TweakDistributions()

  --vanilla
  ContentTweaker.ModifyItemDistribution("GunStoreCounter", "Base.Vest_BulletCivilian", 2)
  ContentTweaker.ModifyItemDistribution("GunStoreShelf", "Base.Vest_BulletCivilian", 2)
  ContentTweaker.ModifyItemDistribution("GarageFirearms", "Base.Vest_BulletCivilian", 0.5)
  ContentTweaker.ModifyItemDistribution("ArmyHangarOutfit", "Base.Vest_BulletArmy", 1)
  ContentTweaker.ModifyItemDistribution("ArmySurplusOutfit", "Base.Vest_BulletCivilian", 4)
  ContentTweaker.ModifyItemDistribution("ArmySurplusOutfit", "Base.Vest_BulletArmy", 0)
  ContentTweaker.ModifyItemDistribution("ArmySurplusMisc", "Base.Vest_BulletCivilian", 0.5)
  ContentTweaker.ModifyItemDistribution("BedroomDresser", "Base.Vest_BulletCivilian", 0.05, true) -- true here means to use the "junk" table
  ContentTweaker.ModifyItemDistribution("WardrobeMan", "Base.Vest_BulletCivilian", 0.05, true)
  ContentTweaker.ModifyItemDistribution("WardrobeManClassy", "Base.Vest_BulletCivilian", 0.05, true)
  ContentTweaker.ModifyItemDistribution("WardrobeWoman", "Base.Vest_BulletCivilian", 0.05, true)
  ContentTweaker.ModifyItemDistribution("WardrobeWomanClassy", "Base.Vest_BulletCivilian", 0.05, true)
  
  
  --USMC Armory (https://steamcommunity.com/sharedfiles/filedetails/?id=2611652130)
  -- interceptor_armor = armor_s w/ shoulderpads arm protection (best)
  -- interceptor_armor_s = armor_v w/ groin (second best)
  -- interceptor_armor_v = vest & neck (third best)
  if getActivatedMods():contains("DRK_1") then    
    --distribution overwrite
    ContentTweaker.RemoveItemsFromAllDistributionTables("Base.Hat_PASGT_Helmet", "Base.Hat_TPASGT_Helmet", "Base.Interceptor_Armor", "Base.Interceptor_Armor_v", "Base.Interceptor_Armor_s", "Base.Marpat_Jacket_w", "Base.Marpat_Jacket_d", "Base.Marpat_Pants_w", "Base.Marpat_Pants_d", "Base.Interceptor_Pouches")
    ContentTweaker.ModifyItemDistributions("ArmyStorageOutfit", {"Base.Interceptor_Armor", 0.2}, {"Base.Hat_TPASGT_Helmet", 1.5}, {"Base.Hat_PASGT_Helmet", 1.5}, {"Base.Hat_Army", 1.5}, {"Base.Interceptor_Armor_s", 0.4}, {"Base.Interceptor_Armor_v", 0.8}, {"Base.Hat_USMC_Cap_w", 5}, {"Base.Hat_USMC_Cap_d", 5}, {"Base.Marpat_Pants_w", 4}, {"Base.Marpat_Pants_d", 4}, {"Base.Marpat_Jacket_w", 4}, {"Base.Marpat_Jacket_d", 4}, {"Base.Interceptor_Pouches", 2})
    ContentTweaker.ModifyItemDistributionsForJunkTable("LockerArmyBedroom", {"Base.Hat_Army", 1.5}, {"Base.Hat_TPASGT_Helmet", 1.5}, {"Base.Hat_PASGT_Helmet", 1.5}, {"Base.Interceptor_Armor", 0.1}, {"Base.Interceptor_Armor_s", 0.25}, {"Base.Interceptor_Armor_v", 0.5}, {"Base.Interceptor_Pouches", 2})
    ContentTweaker.ModifyItemDistributions("LockerArmyBedroom", {"Base.Hat_USMC_Cap_w", 3}, {"Base.Hat_USMC_Cap_d", 3}, {"Base.Marpat_Pants_w", 4}, {"Base.Marpat_Pants_d", 4}, {"Base.Marpat_Jacket_w", 4}, {"Base.Marpat_Jacket_d", 4})
    ContentTweaker.ModifyItemDistributions("ArmySurplusOutfit", {"Base.Hat_USMC_Cap_w", 2}, {"Base.Hat_USMC_Cap_d", 2}, {"Base.Marpat_Pants_w", 4}, {"Base.Marpat_Pants_d", 4}, {"Base.Marpat_Jacket_w", 4}, {"Base.Marpat_Jacket_d", 4}, {"Base.Interceptor_Pouches", 2})
    ContentTweaker.ModifyItemDistributions("ArmyHangarOutfit", {"Base.Hat_USMC_Cap_w", 3}, {"Base.Hat_USMC_Cap_d", 3}, {"Base.Marpat_Pants_w", 3}, {"Base.Marpat_Pants_d", 3}, {"Base.Marpat_Jacket_w", 3}, {"Base.Marpat_Jacket_d", 3}, {"Base.Interceptor_Pouches", 2})
    ContentTweaker.ModifyItemDistribution("PoliceLockers", "Base.Interceptor_Pouches", 1)
    ContentTweaker.ModifyItemDistribution("PoliceStorageOutfit", "Base.Interceptor_Pouches", 2)
    ContentTweaker.ModifyItemDistribution("PoliceStorageGuns", "Base.Interceptor_Pouches", 2)
    ContentTweaker.ModifyItemDistribution("ArmyStorageGuns", "Base.Interceptor_Pouches", 6)
    ContentTweaker.ModifyItemDistribution("GunStoreCounter", "Base.Interceptor_Pouches", 2)
    ContentTweaker.ModifyItemDistribution("GunStoreShelf", "Base.Interceptor_Pouches", 2)
    ContentTweaker.ModifyItemDistribution("ArmySurplusBackpacks", "Base.Interceptor_Pouches", 0.5)
    ContentTweaker.ModifyItemDistribution("ArmySurplusMisc", "Base.Interceptor_Pouches", 0.5)
    ContentTweaker.ModifyItemDistribution("ArmySurplusHeadwear", "Base.Hat_USMC_Cap_w", 5)
    ContentTweaker.ModifyItemDistribution("ArmySurplusHeadwear", "Base.Hat_USMC_Cap_d", 5)
  end
  
  --Operator's M4 (https://steamcommunity.com/sharedfiles/filedetails/?id=2577942794)
  --cleaning kits too common
  if getActivatedMods():contains("OPM") then 
    ContentTweaker.ModifyItemDistribution("GunStoreShelf", "OPM.opm4cleaningkit", 10)
    ContentTweaker.ModifyItemDistribution("FirearmWeapons", "OPM.opm4cleaningkit", 5)
    ContentTweaker.ModifyItemDistribution("ArmyStorageGuns", "OPM.opm4cleaningkit", 30)
    ContentTweaker.ModifyItemDistribution("GunStoreCounter", "OPM.opm4cleaningkit", 2.5)
    ContentTweaker.ModifyItemDistribution("GunStoreDisplayCase", "OPM.opm4cleaningkit", 0)
    ContentTweaker.ModifyItemDistribution("GunStoreDisplayCase", "OPM.OPM4", 0.25)
    ContentTweaker.ModifyItemDistribution("GunStoreShelf", "OPM.OPM4", 0)
    ContentTweaker.ModifyItemDistribution("LockerArmyBedroom", "OPM.opm4cleaningkit", 2.5, true)
    ContentTweaker.ModifyItemDistribution("LockerArmyBedroom", "OPM.OPM4", 0.05, true)
    ContentTweaker.ModifyItemDistribution("ArmyStorageGuns", "OPM.OPM4", 1.5)
  end
end

ContentTweaker.AddItemTweaks(TweakItems)
ContentTweaker.AddDistributionTweaks(TweakDistributions)
